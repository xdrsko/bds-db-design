-- SQLINES DEMO *** rated by MySQL Workbench
-- SQLINES DEMO *** 07 2021
-- SQLINES DEMO ***    Version: 1.0
-- SQLINES DEMO *** orward Engineering

/* SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0; */
/* SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; */
/* SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION'; */

-- SQLINES DEMO *** ------------------------------------
-- Schema mydb
-- SQLINES DEMO *** ------------------------------------

-- SQLINES DEMO *** ------------------------------------
-- Schema mydb
-- SQLINES DEMO *** ------------------------------------
CREATE SCHEMA IF NOT EXISTS mydb ;
SET SCHEMA 'mydb' ;

-- SQLINES DEMO *** ------------------------------------
-- Table mydb.address
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.address_seq;

CREATE TABLE IF NOT EXISTS mydb.address (
  address_id INT NOT NULL DEFAULT NEXTVAL ('mydb.address_seq'),
  street VARCHAR(45) NOT NULL,
  house_number VARCHAR(45) NOT NULL,
  city VARCHAR(45) NOT NULL,
  zip_code VARCHAR(45) NULL,
  country VARCHAR(45) NOT NULL,
  PRIMARY KEY (address_id));


-- SQLINES DEMO *** ------------------------------------
-- Table mydb.contact
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.contact_seq;

CREATE TABLE IF NOT EXISTS mydb.contact (
  contact_id INT NOT NULL DEFAULT NEXTVAL ('mydb.contact_seq'),
  email VARCHAR(45) NOT NULL,
  phone_number VARCHAR(45) NULL,
  PRIMARY KEY (contact_id))
;


-- SQLINES DEMO *** ------------------------------------
-- Table mydb.faculty
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.faculty_seq;

CREATE TABLE IF NOT EXISTS mydb.faculty (
  faculty_id INT NOT NULL UNIQUE DEFAULT NEXTVAL ('mydb.faculty_seq'),
  name VARCHAR(45) NOT NULL,
  address_id INT NOT NULL,
  contact_id INT NOT NULL,
  PRIMARY KEY (faculty_id, address_id, contact_id)
  ,
  CONSTRAINT fk_university_address1
    FOREIGN KEY (address_id)
    REFERENCES mydb.address (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_university_contact1
    FOREIGN KEY (contact_id)
    REFERENCES mydb.contact (contact_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_university_address1_idx ON mydb.faculty (address_id ASC);
CREATE INDEX fk_university_contact1_idx ON mydb.faculty (contact_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ment
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.department_seq;

CREATE TABLE IF NOT EXISTS mydb.department (
  department_id INT NOT NULL UNIQUE DEFAULT NEXTVAL ('mydb.department_seq'),
  name VARCHAR(45) NOT NULL,
  faculty_id INT NOT NULL,
  address_id INT NOT NULL,
  PRIMARY KEY (department_id, faculty_id, address_id)
  ,
  CONSTRAINT fk_department_university1
    FOREIGN KEY (faculty_id)
    REFERENCES mydb.faculty (faculty_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_department_address1
    FOREIGN KEY (address_id)
    REFERENCES mydb.address (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_department_university1_idx ON mydb.department (faculty_id ASC);
CREATE INDEX fk_department_address1_idx ON mydb.department (address_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- Table mydb.user
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.user_seq;

CREATE TABLE IF NOT EXISTS mydb.user (
  user_id INT NOT NULL UNIQUE DEFAULT NEXTVAL ('mydb.user_seq'),
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  contact_id INT NOT NULL,
  PRIMARY KEY (user_id, contact_id)
  ,
  CONSTRAINT fk_user_contact1
    FOREIGN KEY (contact_id)
    REFERENCES mydb.contact (contact_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_user_contact1_idx ON mydb.user (contact_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** programme
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.study_programme_seq;

CREATE TABLE IF NOT EXISTS mydb.study_programme (
  study_programme_id INT NOT NULL DEFAULT NEXTVAL ('mydb.study_programme_seq'),
  title VARCHAR(45) NOT NULL,
  PRIMARY KEY (study_programme_id))
;


-- SQLINES DEMO *** ------------------------------------
-- Table mydb.course
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.course_seq;

CREATE TABLE IF NOT EXISTS mydb.course (
  course_id INT NOT NULL DEFAULT NEXTVAL ('mydb.course_seq'),
  title VARCHAR(45) NOT NULL,
  PRIMARY KEY (course_id))
;


-- SQLINES DEMO *** ------------------------------------
-- Table mydb.role
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.role_seq;

CREATE TABLE IF NOT EXISTS mydb.role (
  role_id INT NOT NULL DEFAULT NEXTVAL ('mydb.role_seq'),
  role_type VARCHAR(45) NOT NULL,
  PRIMARY KEY (role_id))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** as_role
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.user_has_role (
  user_id INT NOT NULL,
  role_id INT NOT NULL,
  PRIMARY KEY (user_id, role_id)
  ,
  CONSTRAINT fk_employee_has_role_employee1
    FOREIGN KEY (user_id)
    REFERENCES mydb.user (user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_employee_has_role_role1
    FOREIGN KEY (role_id)
    REFERENCES mydb.role (role_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_employee_has_role_role1_idx ON mydb.user_has_role (role_id ASC);
CREATE INDEX fk_employee_has_role_employee1_idx ON mydb.user_has_role (user_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** programme_has_course
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.study_programme_has_course_seq;

CREATE TABLE IF NOT EXISTS mydb.study_programme_has_course (
  study_programme_id INT NOT NULL DEFAULT NEXTVAL ('mydb.study_programme_has_course_seq'),
  course_id INT NOT NULL,
  PRIMARY KEY (study_programme_id, course_id)
  ,
  CONSTRAINT fk_study_program_has_course_study_program1
    FOREIGN KEY (study_programme_id)
    REFERENCES mydb.study_programme (study_programme_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_study_program_has_course_course1
    FOREIGN KEY (course_id)
    REFERENCES mydb.course (course_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_study_program_has_course_course1_idx ON mydb.study_programme_has_course (course_id ASC);
CREATE INDEX fk_study_program_has_course_study_program1_idx ON mydb.study_programme_has_course (study_programme_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ment_has_study_programme
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.department_has_study_programme (
  department_id INT NOT NULL UNIQUE,
  study_programme_id INT NOT NULL UNIQUE,
  PRIMARY KEY (department_id, study_programme_id)
  ,
  CONSTRAINT fk_department_has_study_program1_department1
    FOREIGN KEY (department_id)
    REFERENCES mydb.department (department_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_department_has_study_program1_study_program1
    FOREIGN KEY (study_programme_id)
    REFERENCES mydb.study_programme (study_programme_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_department_has_study_program1_study_program1_idx ON mydb.department_has_study_programme (study_programme_id ASC);
CREATE INDEX fk_department_has_study_program1_department1_idx ON mydb.department_has_study_programme (department_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** ment_has_user
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.department_has_user (
  department_id INT NOT NULL,
  user_id INT NOT NULL,
  PRIMARY KEY (department_id, user_id)
  ,
  CONSTRAINT fk_department_has_user_department1
    FOREIGN KEY (department_id)
    REFERENCES mydb.department (department_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_department_has_user_user1
    FOREIGN KEY (user_id)
    REFERENCES mydb.user (user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_department_has_user_user1_idx ON mydb.department_has_user (user_id ASC);
CREATE INDEX fk_department_has_user_department1_idx ON mydb.department_has_user (department_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** as_study_programme
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.user_has_study_programme (
  user_id INT NOT NULL,
  study_programme_id INT NOT NULL,
  PRIMARY KEY (user_id, study_programme_id)
  ,
  CONSTRAINT fk_user_has_study_program_user1
    FOREIGN KEY (user_id)
    REFERENCES mydb.user (user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_user_has_study_program_study_program1
    FOREIGN KEY (study_programme_id)
    REFERENCES mydb.study_programme (study_programme_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_user_has_study_program_study_program1_idx ON mydb.user_has_study_programme (study_programme_id ASC);
CREATE INDEX fk_user_has_study_program_user1_idx ON mydb.user_has_study_programme (user_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** as_course
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.user_has_course (
  user_user_id INT NOT NULL,
  course_course_id INT NOT NULL,
  PRIMARY KEY (user_user_id, course_course_id)
  ,
  CONSTRAINT fk_user_has_course_user1
    FOREIGN KEY (user_user_id)
    REFERENCES mydb.user (user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_user_has_course_course1
    FOREIGN KEY (course_course_id)
    REFERENCES mydb.course (course_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_user_has_course_course1_idx ON mydb.user_has_course (course_course_id ASC);
CREATE INDEX fk_user_has_course_user1_idx ON mydb.user_has_course (user_user_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** info
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.login_info_seq;

CREATE TABLE IF NOT EXISTS mydb.login_info (
  login_info_id INT NOT NULL DEFAULT NEXTVAL ('mydb.login_info_seq'),
  username VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  user_id INT NOT NULL,
  PRIMARY KEY (login_info_id, user_id)
  ,
  CONSTRAINT fk_login_info_user1
    FOREIGN KEY (user_id)
    REFERENCES mydb.user (user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_login_info_user1_idx ON mydb.login_info (user_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- Table mydb.room
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE SEQUENCE mydb.room_seq;

CREATE TABLE IF NOT EXISTS mydb.room (
  room_id INT NOT NULL DEFAULT NEXTVAL ('mydb.room_seq'),
  building VARCHAR(45) NOT NULL,
  floor VARCHAR(45) NOT NULL,
  room_number VARCHAR(45) NOT NULL,
  PRIMARY KEY (room_id))
;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** _has_room
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.course_has_room (
  course_id INT NOT NULL,
  room_id INT NOT NULL,
  PRIMARY KEY (course_id, room_id)
  ,
  CONSTRAINT fk_course_has_room_course1
    FOREIGN KEY (course_id)
    REFERENCES mydb.course (course_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_course_has_room_room1
    FOREIGN KEY (room_id)
    REFERENCES mydb.room (room_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_course_has_room_room1_idx ON mydb.course_has_room (room_id ASC);
CREATE INDEX fk_course_has_room_course1_idx ON mydb.course_has_room (course_id ASC);


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** as_room
-- SQLINES DEMO *** ------------------------------------
-- SQLINES LICENSE FOR EVALUATION USE ONLY
CREATE TABLE IF NOT EXISTS mydb.user_has_room (
  user_id INT NOT NULL,
  room_id INT NOT NULL,
  PRIMARY KEY (user_id, room_id)
  ,
  CONSTRAINT fk_user_has_room_user1
    FOREIGN KEY (user_id)
    REFERENCES mydb.user (user_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_user_has_room_room1
    FOREIGN KEY (room_id)
    REFERENCES mydb.room (room_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
;

CREATE INDEX fk_user_has_room_room1_idx ON mydb.user_has_room (room_id ASC);
CREATE INDEX fk_user_has_room_user1_idx ON mydb.user_has_room (user_id ASC);


/* SET SQL_MODE=@OLD_SQL_MODE; */
/* SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS; */
/* SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; */

START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.address (street, house_number, city, zip_code, country) VALUES ('Technicka 3058', '10', 'Brno', '61600', 'CZ');
INSERT INTO mydb.address (street, house_number, city, zip_code, country) VALUES ('Pukynova 464', '118', 'Brno', '61200', 'CZ');
INSERT INTO mydb.address (street, house_number, city, zip_code, country) VALUES ('Bozetechova 2', '1', 'Brno', '61200', 'CZ');
INSERT INTO mydb.address (street, house_number, city, zip_code, country) VALUES ('Technicka 2896', '2', 'Brno', '61669', 'CZ');
INSERT INTO mydb.address (street, house_number, city, zip_code, country) VALUES ('Veveri 331', '95', 'Brno', '60200', 'CZ');
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.contact
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.contact (email, phone_number) VALUES ('fekt-info@vutbr.cz', '+420 541 141 111');
INSERT INTO mydb.contact (email, phone_number) VALUES ('info@fch.vut.cz', '+420 541 149 301');
INSERT INTO mydb.contact (email, phone_number) VALUES ('info@fit.vut.cz', '+420 541 141 145');
INSERT INTO mydb.contact (email, phone_number) VALUES ('info@fme.vutbr.cz', '+420 541 141 111');
INSERT INTO mydb.contact (email, phone_number) VALUES ('info@fce.vutbr.cz', '+420 541 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('petr_novak@vutbr.cz', '+420 521 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('jana_vitova@vutbr.cz', '+420 543 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('anna_starsgard@vutbr.cz', '+420 541 247 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('hynek_buran@vutbr.cz', '+420 541 167 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('pavla_fouskova@vutbr.cz', '+420 541 157 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('thor_gustafson@vutbr.cz', '+420 541 147 103');
INSERT INTO mydb.contact (email, phone_number) VALUES ('jitka_cermakova@vutbr.cz', '+420 541 147 202');
INSERT INTO mydb.contact (email, phone_number) VALUES ('jana_simanova@vutbr.cz', '+420 541 147 802');
INSERT INTO mydb.contact (email, phone_number) VALUES ('anton@petrvo@vutbr.cz', '+420 541 147 192');
INSERT INTO mydb.contact (email, phone_number) VALUES ('sara_pavelkova@vutbr.cz', '+420 541 147 132');
INSERT INTO mydb.contact (email, phone_number) VALUES ('vojtech_ferry@vutbr.cz', '+420 541 147 172');
INSERT INTO mydb.contact (email, phone_number) VALUES ('miguel_servantes@vutbr.cz', '+420 541 147 142');
INSERT INTO mydb.contact (email, phone_number) VALUES ('vera_litavska@vutbr.cz', '+420 541 147 120');
INSERT INTO mydb.contact (email, phone_number) VALUES ('amal_issa@vutbr.cz', '+420 541 147 119');
INSERT INTO mydb.contact (email, phone_number) VALUES ('renata_polackova@vutbr.cz', '+420 541 147 118');
INSERT INTO mydb.contact (email, phone_number) VALUES ('josef_devic@vutbr.cz', '+420 541 147 117');
INSERT INTO mydb.contact (email, phone_number) VALUES ('handal_tahan@vutbr.cz', '+420 541 147 116');
INSERT INTO mydb.contact (email, phone_number) VALUES ('radek_coufal@vutbr.cz', '+420 541 147 115');
INSERT INTO mydb.contact (email, phone_number) VALUES ('bruno_tasticka@vutbr.cz', '+420 541 147 114');
INSERT INTO mydb.contact (email, phone_number) VALUES ('zita_fialova@vutbr.cz', '+420 541 113 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('radek_blazek@vutbr.cz', '+420 541 114 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('hana_novotna@vutbr.cz', '+420 241 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('aziz_badawi@vutbr.cz', '+420 231 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('pavel_jonak@vutbr.cz', '+420 551 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('zdenek_jandasekk@vutbr.cz', '+420 561 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('ilona_havelkova@vutbr.cz', '+420 571 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('karel_felda@vutbr.cz', '+420 581 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('adam_jelinek@vutbr.cz', '+420 591 147 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('ota_kolecko@vutbr.cz', '+420 541 150 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('tereza_mackova@vutbr.cz', '+420 541 151 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('milos_tahlo@vutbr.cz', '+420 541 152 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('anna_dobra@vutbr.cz', '+420 541 153 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('daniel_decky@vutbr.cz', '+420 541 154 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('filip_galon@vutbr.cz', '+420 541 155 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('tomas_fanda@vutbr.cz', '+420 541 156 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('edita_salamounova@vutbr.cz', '+420 541 169 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('renata_unikalova@vutbr.cz', '+420 541 170 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('jerry_hill@vutbr.cz', '+420 541 171 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('david_hajny@vutbr.cz', '+420 541 172 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('anton_ivanov@vutbr.cz', '+420 541 172 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('alexandra_tomaskova@vutbr.cz', '+420 541 173 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('eliska_wernerova@vutbr.cz', '+420 541 174 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('pavel_kolar@vutbr.cz', '+420 541 175 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('zdenek_janacek@vutbr.cz', '+420 541 176 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('klara_kolarova@vutbr.cz', '+420 541 177 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('katerina_gardenova@vutbr.cz', '+420 541 178 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('baz_nassir@vutbr.cz', '+420 541 179 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('karel_kolacek@vutbr.cz', '+420 541 180 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('wang_chun@vutbr.cz', '+420 541 181 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('sara_zelenkova@vutbr.cz', '+420 541 182 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('karolina_opletalova@vutbr.cz', '+420 541 183 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('svetlana_ivanovna@vutbr.cz', '+420 541 184 102');
INSERT INTO mydb.contact (email, phone_number) VALUES ('jan_pavelka@vutbr.cz', '+420 141 124 112');
INSERT INTO mydb.contact (email, phone_number) VALUES ('karolina_novotna@vutbr.cz', '+420 841 484 502');
INSERT INTO mydb.contact (email, phone_number) VALUES ('bohumil_tatarka@vutbr.cz', '+420 540 190 125');
INSERT INTO mydb.contact (email, phone_number) VALUES ('julie_spoustova@vutbr.cz', '+420 540 101 165');
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.faculty
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.faculty (name, address_id, contact_id) VALUES ('FEEC', 1, 1);
INSERT INTO mydb.faculty (name, address_id, contact_id) VALUES ('FCH', 2, 2);
INSERT INTO mydb.faculty (name, address_id, contact_id) VALUES ('FIT', 3, 3);
INSERT INTO mydb.faculty (name, address_id, contact_id) VALUES ('FME', 4, 4);
INSERT INTO mydb.faculty (name, address_id, contact_id) VALUES ('FEEC', 5, 5);
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.department
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.department (name, faculty_id, address_id) VALUES ('UTKO', 1, 1);
INSERT INTO mydb.department (name, faculty_id, address_id) VALUES ('UFSCH', 2, 2);
INSERT INTO mydb.department (name, faculty_id, address_id) VALUES ('UIFS', 3, 3);
INSERT INTO mydb.department (name, faculty_id, address_id) VALUES ('UM', 4, 4);
INSERT INTO mydb.department (name, faculty_id, address_id) VALUES ('UEEN', 5, 5);

COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.user
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Petr', 'Novak', 6);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('RNDr.Jana', 'Vitova,CSc', 7);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Anna', 'Starsgard, Ph.D', 8);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing. Hynek', 'Buran', 9);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Mgr.Thor', 'Gustafson', 10);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Jitka', 'Cermakova, Ph.D', 11);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing. Jana', 'Simanova', 12);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing. Anton', 'Petrov,Ph.D', 13);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Sara', 'Pavelkova', 14);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr. Vojtech', 'Ferry', 15);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Miguel', 'Servantes.M.S', 16);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing. Vera', 'Litavska,Ph.D', 17);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Eng.Amal', 'Issa,Ph.D', 18);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Renata', 'Polackova', 19);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('RNDr.Josef', 'Devic', 20);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Handal', 'Tahann,M.S', 21);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Radek', 'Coufal, Ph.D', 22);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Bruno', 'Tasticka', 23);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Mgr.Zita', 'Fialova', 24);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Radek', 'Blazek', 25);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing. Hana', 'Novotna,Ph.D', 26);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Aziz', 'Badawi M.S', 27);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('RNDr. Pavel', 'Jonak,CSc', 28);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('doc.RNDr.Zdenek', 'Jandasek', 29);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Ilona', 'Havelkova,Ph.D', 30);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('doc.Ing.Karel', 'Felda Ph.D', 31);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Adam', 'Jelinek', 32);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Ota', 'Kolecko', 33);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('doc.Ing.Tereza', 'Mackova Ph.D', 34);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Milos', 'Tahlo', 35);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Mgr.Anna', 'Dobra', 36);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('doc.Ing.David', 'Decky', 37);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('RNDr.Filip', 'Galon,CSc', 38);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Tomas', 'Fanda', 39);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Edita', 'Salamounova', 40);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Renata', 'Unikalova', 41);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Jerry', 'Hill,M.S', 42);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.David', 'Hajny', 43);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Anton', 'Ivanov', 44);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Alexandra', 'Tomaskova', 45);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('RNDr.Eliska', 'Wernerova,CSc', 46);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('doc.RNDr.Pavel', 'Kolar', 47);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Zdenek', 'Janacek', 48);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Klara', 'Kolarova', 49);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Katerina ', 'Gardenova,Ph.d', 50);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Baz', 'Nassir,M.S', 51);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Mgr.Karel', 'Kolacek', 52);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Ing.Wang', 'Chun,Ph.D', 53);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('doc.Ing. Sara', 'Zelenkova,Ph.D', 54);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr.Karolina', 'Opletalova', 55);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Mgr. Svetlana', 'Ivanovna', 56);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Jan', 'Pavelka', 57);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Karolina', 'Novotna', 58);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Bohumil', 'Tatarka', 59);
INSERT INTO mydb.user (first_name, last_name, contact_id) VALUES ('Julie', 'Stpoustova', 60);
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.study_programme
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.study_programme (title) VALUES ('BPC-IBE');
INSERT INTO mydb.study_programme (title) VALUES ('BPCP_CHCHTE');
INSERT INTO mydb.study_programme (title) VALUES ('BIT');
INSERT INTO mydb.study_programme (title) VALUES ('B-MAI-P');
INSERT INTO mydb.study_programme (title) VALUES ('BPC-SEE');
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.course
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.course (title) VALUES ('BPC-KOM');
INSERT INTO mydb.course (title) VALUES ('BPC-MA1');
INSERT INTO mydb.course (title) VALUES ('BPC-PC1T');
INSERT INTO mydb.course (title) VALUES ('BPC-ZKR');
INSERT INTO mydb.course (title) VALUES ('BPC-DMA');
INSERT INTO mydb.course (title) VALUES ('BPC-AKR');
INSERT INTO mydb.course (title) VALUES ('BPC-PST');
INSERT INTO mydb.course (title) VALUES ('BPC-DAK');
INSERT INTO mydb.course (title) VALUES ('BPC-TIN');
INSERT INTO mydb.course (title) VALUES ('BPC-CPT');
INSERT INTO mydb.course (title) VALUES ('BC_CHI1');
INSERT INTO mydb.course (title) VALUES ('BC_MAT1');
INSERT INTO mydb.course (title) VALUES ('BC_FYZ1');
INSERT INTO mydb.course (title) VALUES ('BC_MAT2');
INSERT INTO mydb.course (title) VALUES ('BC_OBT');
INSERT INTO mydb.course (title) VALUES ('BC_ANC1');
INSERT INTO mydb.course (title) VALUES ('BC_OCH2');
INSERT INTO mydb.course (title) VALUES ('BC_FYZ2');
INSERT INTO mydb.course (title) VALUES ('BC_EMT');
INSERT INTO mydb.course (title) VALUES ('BC_BCH1');
INSERT INTO mydb.course (title) VALUES ('ILG');
INSERT INTO mydb.course (title) VALUES ('IMA1');
INSERT INTO mydb.course (title) VALUES ('IAL');
INSERT INTO mydb.course (title) VALUES ('IPT');
INSERT INTO mydb.course (title) VALUES ('IDS');
INSERT INTO mydb.course (title) VALUES ('IPP');
INSERT INTO mydb.course (title) VALUES ('IPK');
INSERT INTO mydb.course (title) VALUES ('IIS');
INSERT INTO mydb.course (title) VALUES ('ISA');
INSERT INTO mydb.course (title) VALUES ('ISS');
INSERT INTO mydb.course (title) VALUES ('IJC');
INSERT INTO mydb.course (title) VALUES ('3CD');
INSERT INTO mydb.course (title) VALUES ('1KG');
INSERT INTO mydb.course (title) VALUES ('1K');
INSERT INTO mydb.course (title) VALUES ('2F');
INSERT INTO mydb.course (title) VALUES ('SOA');
INSERT INTO mydb.course (title) VALUES ('3F');
INSERT INTO mydb.course (title) VALUES ('3ST');
INSERT INTO mydb.course (title) VALUES ('VDS');
INSERT INTO mydb.course (title) VALUES ('SU1');
INSERT INTO mydb.course (title) VALUES ('4PP');
INSERT INTO mydb.course (title) VALUES ('BPC-EL1');
INSERT INTO mydb.course (title) VALUES ('BPC-PC1S');
INSERT INTO mydb.course (title) VALUES ('BPC-EL2');
INSERT INTO mydb.course (title) VALUES ('BPC-PC2S');
INSERT INTO mydb.course (title) VALUES ('BPC-DEE');
INSERT INTO mydb.course (title) VALUES ('BPC-VEE');
INSERT INTO mydb.course (title) VALUES ('BPC-TRB');
INSERT INTO mydb.course (title) VALUES ('BPC-EPB');
INSERT INTO mydb.course (title) VALUES ('BPC-RZB');
INSERT INTO mydb.course (title) VALUES ('BPC-ANA');
INSERT INTO mydb.course (title) VALUES ('BPC-SNI');
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.role
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('employee');
INSERT INTO mydb.role (role_type) VALUES ('student');
INSERT INTO mydb.role (role_type) VALUES ('student');
INSERT INTO mydb.role (role_type) VALUES ('student');
INSERT INTO mydb.role (role_type) VALUES ('student');
COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.login_info
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.login_info (username, password, user_id) VALUES ('novak', 'petr', 1);
INSERT INTO mydb.login_info (username, password, user_id) VALUES ('vitova', 'jana', 2);
INSERT INTO mydb.login_info (username, password, user_id) VALUES ('starsgard', 'anna', 3);
INSERT INTO mydb.login_info (username, password, user_id) VALUES ('buran', 'hynek', 4);
INSERT INTO mydb.login_info (username, password, user_id) VALUES ('gustafson', 'thor', 5);

COMMIT;


-- SQLINES DEMO *** ------------------------------------
-- SQLINES DEMO *** db.room
-- SQLINES DEMO *** ------------------------------------
START TRANSACTION;
SET SCHEMA 'mydb';
INSERT INTO mydb.room (building, floor, room_number) VALUES ('T12', '2', '01');
INSERT INTO mydb.room (building, floor, room_number) VALUES ('T12', '2', '02');
INSERT INTO mydb.room (building, floor, room_number) VALUES ('T12', '2', '03');
INSERT INTO mydb.room (building, floor, room_number) VALUES ('T12', '2', '04');
INSERT INTO mydb.room (building, floor, room_number) VALUES ('T12', '2', '05');

COMMIT;


